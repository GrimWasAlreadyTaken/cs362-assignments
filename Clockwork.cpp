#include <iostream>

//Author: Justynn Palmer

using namespace std;

int main()
{
    int increment = 0;
    
    while(increment != -1) {
        cin >> increment;
        
        int minutes = 720;
        
        while((minutes % increment) != 0) {
            minutes += 720;
        }
        
        cout << minutes << endl;
    }
    
return 0;
}